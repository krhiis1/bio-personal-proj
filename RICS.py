import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d

os.chdir("/Users/hiis-/Desktop/HDF5/Bio")

filename = 'proov2_RICS.h5'
f = h5py.File(filename, 'r')

data = f.get("ImageStream/Confocal/Images/0")

data1 = list(data.keys())
el = len(data1)

b = data[data1[0]].shape

a1 = []
a2 = []
a3 = []

c= 0
d= 0

def padding(data):
    u = []
    for i in range(len(data)):
        paddeddata = np.pad(data[i],((data[i].shape[0]//2,data[i].shape[0]//2),(data[i].shape[1]//2,data[i].shape[1]//2)), constant_values=0.0)
        u.append(paddeddata)
    return u

def calcmean(data1, data2, data3):
    #Sum all the groups
    sum1 = np.sum(data1)
    sum2 = np.sum(data2)
    sum3 = np.sum(data3)
    #b,c,d lists using the shapes of a1,a2,a3 
    leng = data1[0].shape[0]*data1[0].shape[1]*len(data1)+data2[0].shape[0]*data2[0].shape[1]*len(data2)+data3[0].shape[0]*data3[0].shape[1]*len(data3)
    mean = (sum1 + sum2 + sum3)/leng
    return mean

def factorcalc(data):
    yhed = np.ones((data[0].shape[0]//2,data[0].shape[1]//2))
    yhed = np.pad(yhed,((yhed.shape[0]//2,yhed.shape[0]//2),(yhed.shape[1]//2,yhed.shape[1]//2)), constant_values=0.0)
    factor = signal.correlate(yhed,yhed,mode="same")#[yhed.shape[0]//2:,yhed.shape[1]//2:]
    return factor

def correlationcalc(data,factor):
    at = []
    for i in range(len(data)):
        corre = signal.correlate(data[i],data[i],mode="same")#[data[i].shape[0]//2:,data[i].shape[1]//2:]
        corre = corre/factor
        at.append(corre)
    return at

def picturemean(data):
    picturemean = np.mean(data,axis=0)
    return picturemean

for i in range(el):
    if data[data1[i]].shape ==b:
        a1x = data[data1[i]].attrs["PixelSizeX"]
        a1y = data[data1[i]].attrs["PixelSizeY"]
        paddata = data[data1[i]][:].astype(np.float).T
        a1.append(paddata)
    else:
        c = data[data1[i]].shape
        break
    
        
for i in range(len(a1),el):
    if data[data1[i]].shape ==c:
        a2x = data[data1[i]].attrs["PixelSizeX"]
        a2y = data[data1[i]].attrs["PixelSizeY"]
        paddata1 = data[data1[i]][:].astype(np.float).T
        a2.append(paddata1)
    else:
        d = data[data1[i]].shape
        break
    
for i in range(len(a2),el):
    if data[data1[i]].shape == d:
        a3x = data[data1[i]].attrs["PixelSizeX"]
        a3y = data[data1[i]].attrs["PixelSizeY"]
        paddata2 = data[data1[i]][:].astype(np.float).T
        a3.append(paddata2)

mean = calcmean(a1,a2,a3)

picmean1 = picturemean(a1)
picmean2 = picturemean(a2)
picmean3 = picturemean(a3)

a1 = a1 - picmean1
a2 = a2 - picmean2
a3 = a3 - picmean3

a1 = padding(a1)
a2 = padding(a2)
a3 = padding(a3)

print(mean)
print(b)
print(c)
print(d)

#listi alguseks
a1t = []
a2t = []
a3t = []

factor = factorcalc(a1)
factor1 = factorcalc(a2)
factor2 = factorcalc(a3)

a1t = correlationcalc(a1,factor)
a2t = correlationcalc(a2,factor1)
a3t = correlationcalc(a3,factor2)

a1sum = np.mean(a1t,axis=0) #sum(a1t)/len(a1t)
a2sum = np.mean(a2t,axis=0) #sum(a2t)/len(a2t)
a3sum = np.mean(a3t,axis=0) #sum(a3t)/len(a3t)

a1sum = a1sum/mean**2
a2sum = a2sum/mean**2
a3sum = a3sum/mean**2

#a1sum = a1sum.T
#a2sum = a2sum.T
#a3sum = a3sum.T

plt.imshow(a1sum, vmin = 0 , vmax = 1)
plt.figure("2")
plt.imshow(a2sum, vmin = 0 , vmax = 1)
plt.figure("3")
plt.imshow(a3sum, vmin = 0 , vmax = 1)

x1 = np.arange(0,a1sum.shape[0]//2)
plt.figure("a1")
plt.title((b,filename[:6]))
plt.plot(x1,a1sum[a1sum.shape[0]//2:,a1sum.shape[1]//2])
plt.ylabel("Normalized Correlation")
plt.xlabel("Lag")
plt.xscale("log")
plt.ylim(0,3)

x2 = np.arange(0,a2sum.shape[0]//2)
plt.figure("a2")
plt.title((c,filename[:6]))
plt.plot(x2,a2sum[a2sum.shape[0]//2:,a2sum.shape[1]//2])
plt.ylabel("Normalized Correlation")
plt.xlabel("Lag")
plt.xscale("log")
plt.ylim(0,3)

x3 = np.arange(0,a3sum.shape[0]//2)
plt.figure("a3")
plt.title((d,filename[:6]))
plt.plot(x3,a3sum[a3sum.shape[0]//2:,a3sum.shape[1]//2])
plt.ylabel("Normalized Correlation")
plt.xlabel("Lag")
plt.xscale("log")
plt.ylim(0,3)
