import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv


os.chdir("/Users/hiis-/Desktop/HDF5/Bio")

filename = 'proov1_RICS.h5'
f = h5py.File(filename, 'r')

data = f.get("ImageStream/Confocal/Images/0")

data1 = list(data.keys())
el = len(data1)

b = data[data1[0]].shape

protocolrics = f.get("ProtocolConfocalRICS/rics")
linetime = protocolrics["line scan time"]
flyback = protocolrics["flyback"]

a1 = []
a2 = []
a3 = []

c= 0
d= 0

def padding(data):
    u = []
    for i in range(len(data)):
        paddeddata = np.pad(data[i],((data[i].shape[0]//2,data[i].shape[0]//2),(data[i].shape[1]//2,data[i].shape[1]//2)), constant_values=0.0)
        u.append(paddeddata)
    return u

def calcmean(data1, data2, data3):
    #Sum all the groups
    sum1 = np.sum(data1)
    sum2 = np.sum(data2)
    sum3 = np.sum(data3)
    #b,c,d lists using the shapes of a1,a2,a3 
    leng = data1[0].shape[0]*data1[0].shape[1]*len(data1)+data2[0].shape[0]*data2[0].shape[1]*len(data2)+data3[0].shape[0]*data3[0].shape[1]*len(data3)
    mean = (sum1 + sum2 + sum3)/leng
    return mean 

def factorcalc(data):
    yhed = np.ones((data[0].shape[0]//2,data[0].shape[1]//2))
    yhed = np.pad(yhed,((yhed.shape[0]//2,yhed.shape[0]//2),(yhed.shape[1]//2,yhed.shape[1]//2)), constant_values=0.0)
    factor = signal.correlate(yhed,yhed,mode="same")
    return factor

def correlationcalc(data,factor):
    at = []
    for i in range(len(data)):
        corre = signal.correlate(data[i],data[i],mode="same")
        corre = corre/factor
        at.append(corre)
    return at

def picturemean(data):
    picturemean = np.mean(data,axis=0)
    return picturemean

def dataset(size):
    a1 = []
    for i in range(el):
        if data[data1[i]].shape == size:
            pixelsizex = data[data1[i]].attrs["PixelSizeX"]
            pixelsizey = data[data1[i]].attrs["PixelSizeY"]
            linet = linetime[i]
            dataset = data[data1[i]][:].astype(np.float).T
            pixelt = linet/dataset.shape[0]
            linet = linet*(1 + flyback[i])
            a1.append(dataset)
    return a1, linet, pixelt, pixelsizex , pixelsizey

def datasize():
    a = 0
    shape1 = data[data1[0]].shape
    for i in range(el):
        if data[data1[i]].shape == shape1:
            a = i
        else:
            shape2 = data[data1[i]].shape
            shape3 = data[data1[el-1]].shape
            break
    #for i in range(a,el):
           # if data[data1[i]].shape == shape2:
            #    a += i
           # else:
              #  shape3 = data[data1[i]].shape
               # break
    return shape1, shape2, shape3

sh1, sh2, sh3 = datasize()

a1, tl1, tp1, a1x, a1y = dataset(sh1)
a2, tl2, tp2, a2x, a2y = dataset(sh2)
a3, tl3, tp3, a3x, a3y = dataset(sh3)

mean = calcmean(a1,a2,a3)

picmean1 = picturemean(a1)
picmean2 = picturemean(a2)
picmean3 = picturemean(a3)

a1 = a1 - picmean1
a2 = a2 - picmean2
a3 = a3 - picmean3

a1 = padding(a1)
a2 = padding(a2)
a3 = padding(a3)

#print(mean)

#listi alguseks
a1t = []
a2t = []
a3t = []

factor = factorcalc(a1)
factor1 = factorcalc(a2)
factor2 = factorcalc(a3)

a1t = correlationcalc(a1,factor)
a2t = correlationcalc(a2,factor1)
a3t = correlationcalc(a3,factor2)

#%% proov1 remove the irregular images

indexes1 = [2,5,8,14,16,24]
for index in sorted(indexes1, reverse=True):
    del a1t[index]
  
indexes2 = [2,11,12,13,14,22,28,33,64,67,68,83,85]
for index in sorted(indexes2, reverse=True):
    del a2t[index]
    
indexes3 = [0,1,3,8,14,24,26,33,53,55,56]
for index in sorted(indexes3, reverse=True):
    del a3t[index]

#%%proov2

#indexes1 = [10,12,24,25,26,27,32,42,47,49,50,53,58,60,61]
#for index in sorted(indexes1, reverse=True):
#    del a1t[index]
  
#indexes2 = [1,3,12,26,27,28,30,39,41,43,50,51]
#for index in sorted(indexes2, reverse=True):
#    del a2t[index]
    
#indexes3 = [13,25,26]
#for index in sorted(indexes3, reverse=True):
#    del a3t[index]

#%% proov3

#indexes1 = [0,1,2,3,4,6,8,9,10,11,12,14,15,16,22,25,30,31,32,33,38,39,40,44,45,47,49,50,53,61,62,64,65,69,70,73,75,85,87]
#for index in sorted(indexes1, reverse=True):
#    del a1t[index]
  
#indexes2 = [1,2,4,7,8,11,12,13,14,15,19,21,22,25,26,27,29,30,32,35,37,38,41,42,43,44,46,47,54,56,57]
#for index in sorted(indexes2, reverse=True):
#    del a2t[index]
    
#indexes3 = [0,1,2,5,6,7,9,10,11,15,20,23,26,27,28,29]
#for index in sorted(indexes3, reverse=True):
#    del a3t[index]

#%% proov4

#indexes1 = [0,11,16,17,19,21,24,27,36,40,42,43,46,47,48,49,50,51,58,59,60,63,66,71,72,74,76,80,81,83]
#for index in sorted(indexes1, reverse=True):
#    del a1t[index]
  
#indexes2 = [0,1,3,5,7,12,17,37,38,39,44,45,47,49,52,53,54,55,57,58,59]
#for index in sorted(indexes2, reverse=True):
#    del a2t[index]
    
#indexes3 = [6,9,12,22,28]
#for index in sorted(indexes3, reverse=True):
#    del a3t[index]

#indexes1 = [15,34,35,48,54]
#for index in sorted(indexes1, reverse=True):
#    del a1t[index]   

#%% Proov5

#indexes1 = [0,5,16,35,70,81,87]
#for index in sorted(indexes1, reverse=True):
#    del a1t[index]
  
#indexes2 = [0,7,9,15,34,39,41,44,45,48,51,57]
#for index in sorted(indexes2, reverse=True):
#    del a2t[index]

#Mean the images

a1sum = np.mean(a1t,axis=0) 
a2sum = np.mean(a2t,axis=0) 
a3sum = np.mean(a3t,axis=0) 

a1sum = a1sum/mean**2
a2sum = a2sum/mean**2
a3sum = a3sum/mean**2

#Preparing saving the files

a1sumx,a1sumy= a1sum.shape
a2sumx,a2sumy = a2sum.shape
a3sumx,a3sumy = a3sum.shape

a1sum1 = a1sum
a2sum2 = a2sum
a3sum3 = a3sum

a1suml = a1sum1.flatten()
a2suml = a2sum2.flatten()
a3suml = a3sum3.flatten()

a1suml=list(a1suml)
a2suml=list(a2suml)
a3suml=list(a3suml)


#Saving files as _parameters and _data

header = ["Data1","Data2","Data3","Sizex1", "Sizey1","Sizex2", "Sizey2","Sizex3", "Sizey3"]

data = [
        [ a1suml,a2suml,a3suml, a1sumx,a1sumy,a2sumx,a2sumy,a3sumx,a3sumy]#,
        #[ a2suml, tl2, tp2, a2x, a2y,a2sumx,a2sumy],
        #[ a3suml, tl3, tp3, a3x, a3y,a3sumx,a3sumy],
        ]

with open(str(filename[:6])+"_data.csv","w", encoding = "UTF8", newline = "") as f:
    writer = csv.writer(f, delimiter = ",")
    
    writer.writerow(header)
    
    writer.writerows(data)


header1 = ["Linetime","Pixeltime","Pixelsizex","Pixelsizey"]

data1 = [
        [tl1, tp1, a1x, a1y],
        [tl2, tp2, a2x, a2y],
        [tl3, tp3, a3x, a3y],
        ]

with open(str(filename[:6])+"_parameters.csv","w", encoding = "UTF8", newline = "") as f:
    writer = csv.writer(f, delimiter = ",")
    
    writer.writerow(header1)
    
    writer.writerows(data1)
