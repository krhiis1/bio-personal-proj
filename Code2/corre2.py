import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv
import argparse
from dataseparate import dataseparater
import pickle

#Functions


def padding(data):
    u = []
    for i in range(len(data)):
        paddeddata = np.pad(data[i].image, (
        (data[i].image.shape[0] // 2, data[i].image.shape[0] // 2), (data[i].image.shape[1] // 2, data[i].image.shape[1] // 2)),
                            constant_values=0.0)
        u.append(paddeddata)
    return u

def factorcalc(data):
    factor = []
    for i in range(len(data)):
        yhed = np.ones((data[i].shape[0] // 2, data[i].shape[1] // 2))
        yhed = np.pad(yhed, ((yhed.shape[0] // 2, yhed.shape[0] // 2), (yhed.shape[1] // 2, yhed.shape[1] // 2)),
                      constant_values=0.0)
        factor.append(signal.correlate(yhed, yhed, mode="same"))
    return factor

#def calcmean(data1, data2, data3): #Not in use atm
#    sum1 = np.sum(data1)
#    sum2 = np.sum(data2)
#    sum3 = np.sum(data3)
    # b,c,d lists using the shapes of a1,a2,a3
#    leng = data1[0].shape[0] * data1[0].shape[1] * len(data1) + data2[0].shape[0] * data2[0].shape[1] * len(data2) + \
#           data3[0].shape[0] * data3[0].shape[1] * len(data3)
#    mean = (sum1 + sum2 + sum3) / leng
#    return mean



def correlationcalc(data, infodata, factor):
    for i in range(len(data)):
        corre = signal.correlate(data[i], data[i], mode="same")
        corre = corre / factor
        infodata[i].corre = corre
    return None

def datasmaller(data, N): #have to redo, if any other splitting is wanted
    a = data
    c = np.vsplit(a, 2)
    b1, b11 = np.hsplit(c[0], 2)
    b2, b22 = np.hsplit(c[1], 2)
    u1 = b1
    u2 = b2
    u3 = b11
    u4 = b22
    return u1, u2, u3, u4

def picturemean(data):
    picturemean = np.mean(data, axis=0)
    return picturemean

def data_out(data,el):
    #a = 0
    #shape1 = data[data1[0]].shape # grab the shape of the first data image
    shape1list = []
    #print('fj')
    #print(el)

    for i in range(el):
        #grab the base data
        pixelsizex = data[data1[i]].attrs["PixelSizeX"]
        pixelsizey = data[data1[i]].attrs["PixelSizeY"]
        linet = linetime[i]
        dataset = data[data1[i]][:].astype(float).T
        a1, a2, a3, a4 = datasmaller(dataset,4)

        pixelt = linet / dataset.shape[0]
        linet = linet * (1 + flyback[i])
        #mean = np.mean(dataset)
        shape = data[data1[i]].shape

        shape1list.append(dataseparater(a1, pixelsizex, pixelsizey, linet, pixelt, shape))
        shape1list.append(dataseparater(a2, pixelsizex, pixelsizey, linet, pixelt, shape))
        shape1list.append(dataseparater(a3, pixelsizex, pixelsizey, linet, pixelt, shape))
        shape1list.append(dataseparater(a4, pixelsizex, pixelsizey, linet, pixelt, shape))

    return shape1list


#Read in the data


#Read in the data
parser = argparse.ArgumentParser(description='Read in the filename')
parser.add_argument('filepath', type= str)
args = parser.parse_args()

filename = args.filepath


#filename = 'proov5_RICS.h5'
f = h5py.File(filename, 'r')


data = f.get("ImageStream/Confocal/Images/0")
data1 = sorted(list(data.keys()))

filename = args.filepath
el = len(data1)

b = data[data1[0]].shape

protocolrics = f.get("ProtocolConfocalRICS/rics")
linetime = protocolrics["line scan time"]
flyback = protocolrics["flyback"]

print(data[data1[0]])

dataout = data_out(data,el)
print(len(dataout))

S,C = 0,0
for i in dataout:
    s,c = i.calcsum()
    S += s
    C += c

mean = S/C
print(mean)

u = 0
for i in dataout:
    #print(u)
    i.padding(mean)
    i.mean = mean
    i.correlationcalc()
    u +=1

pickle.dump(dataout, open(filename[:-3]+"_calcdata", "wb"))

#calculate mean, subtract mean, pad, calculate factor, calculate correlation, save data

#paddata = padding(dataout)

#factor = factorcalc(paddata)

#correlationcalc(paddata,dataout,factor) #paddata for correlations, dataout for class
