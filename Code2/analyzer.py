import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv
import argparse
from dataseparate import dataseparater
import pickle
import collections
from matplotlib.backends.backend_pdf import PdfPages


parser = argparse.ArgumentParser(description='Read in the filename')
parser.add_argument('filepath', type= str)
parser.add_argument('datavar', metavar = 'N.N', type= float, nargs = '?', const = -1, default =2)
args = parser.parse_args()

filename = args.filepath
variable = args.datavar

data = pickle.load(open(filename[:-3]+"_calcdata", "rb"))

#self.image = data
#self.pixelsizex = pixelsizex
#self.pixelsizey = pixelsizey
#self.linet = linet
#self.pixelt = pixelt
#self.shape = shape
#self.mean = None
#self.corre = None



#for i in data:
#    print(i.shape)
#    print(i.pixelsizex,i.pixelsizey,i.linet, i.pixelt, i.mean, i.corre)


def diffusiondata(data, linetime, pixeltime, pixelsizex, pixelsizey, N):
    xi = np.arange(0, NN)
    x = np.concatenate([xi]*N)#np.concatenate((x, x, x, x))
    x = x.astype(np.float)
    #bkiga
    #print("test")
    #print(len(x))
    yy=[]
    for i in range(N):
        yy.append(i*np.ones(NN))

    #y = yy
    y = np.concatenate(yy)#np.concatenate((0 * np.ones(NN), 1 * np.ones(NN), 2 * np.ones(NN), 3 * np.ones(NN)))
    y= y.astype(np.float)
    #print(len(y))

    corr = data[data.shape[0] // 2:data.shape[0] // 2 + NN,data.shape[1] // 2:data.shape[1] // 2 + N].ravel(order="F")

    corr = corr[1:]
    x = x[1:]
    y = y[1:]
    #print("test")
    #print(len(x))
    #print(len(y))
    #print(len(pixeltime))
    #print(type(linetime))

    T2 = pixeltime * x + linetime * y

    tx = pixelsizex * x
    ty = pixelsizey * y

    T1 = np.square(tx) + np.square(ty) #tx ** 2 + ty ** 2

    return corr, T2, T1, x
#self.image = data
#self.pixelsizex = pixelsizex
#self.pixelsizey = pixelsizey
#self.linet = linet
#self.pixelt = pixelt
#self.shape = shape
#self.mean = None
#self.corre = None

NN = 120 # how many pixels from the center
NI = 4

dif = []
filterN = []

class Parameters:
    def __init__(self, c):
        #self.a = a
        #self.b = b
        self.c = c

    def out(self):
        #a = self.a
        #b = self.b
        c = self.c
        N3 = c[0]
        D13 = np.exp(c[1])  # np.exp(c[1])#np.arctan(c[1])/np.pi+0.5
        #D23 = (np.arctan(c[2]) / np.pi + 0.5) * 200 + 1.5 * D13  # (np.arctan(c[2])/np.pi+0.5)*1000 + 1.5*D13
        tripfr2 = np.arctan(c[2]) / np.pi + 0.5
        triptau2 = np.exp(c[3])
        y2 = np.arctan(c[4]) / np.pi + 0.5
        #p1 = (len(div1)+len(div2)+len(div3))/(len(a1t)+len(a2t)+len(a3t))
        g = np.asarray((N3, D13, tripfr2, triptau2, y2))
        return g

    def out2(self):
        #a = self.a
        #b = self.b
        c = self.c
        N3 = c[0]
        D13 = np.exp(c[1])  # np.exp(c[1])#np.arctan(c[1])/np.pi+0.5
        D23 = (np.arctan(c[2]) / np.pi + 0.5) * 200 + 1.5 * D13  # (np.arctan(c[2])/np.pi+0.5)*1000 + 1.5*D13
        tripfr2 = np.arctan(c[3]) / np.pi + 0.5
        triptau2 = np.exp(c[4])
        y2 = np.arctan(c[5]) / np.pi + 0.5
       # p1 = (len(div1)+len(div2)+len(div3))/(len(a1t)+len(a2t)+len(a3t))
        g = np.asarray((N3, D13, D23, tripfr2, triptau2, y2))
        return g

    def print(self, g, sum, text):
        print('Fit data:')
        print('N ='+ str(g[0]))
        print('Diffusion 1 =' + str(g[1]))
        #print('Diffusion 2 =' + str(g[2]))
        print('Triplet fraction =' + str(g[2]))
        print('Triplet state time=' + str(g[3]))
        print('Diffusion fraction =' + str(g[4]))

    def print2(self, g, sum, text):
        print('Fit data:')
        print('N ='+ str(g[0]))
        print('Diffusion 1 =' + str(g[1]))
        print('Diffusion 2 =' + str(g[2]))
        print('Triplet fraction =' + str(g[3]))
        print('Triplet state time=' + str(g[4]))
        print('Diffusion fraction =' + str(g[5]))


class Task:  # N,D1,D2,tripfr,triptau,Y
    def __init__(self, T1, T2):
        self.T1 = T1
        self.T2 = T2
        self.N = []
        self.D1 = []
        self.D2 = []
        self.tripfr = []
        self.triptau = []
        self.Y = []

    def target1(self, x, N, dd1, ttripfr, ttriptau, y):
        # w = 0.35#mikromeeter
        alpha = 4  # ilma ühikuta
        # dx = a1x #mikromeeter
        # dy = a1y #mikromeeter
        # tp = np.float(tl1) #dwell time s
        # tl = np.float(tp1) #line time s
        w = 0.2856  # 0.26
        D1 = np.exp(dd1)  # np.exp(dd1)#np.arctan(dd1)/np.pi+0.5
        #D2 = (np.arctan(dd2) / np.pi + 0.5) * 200 + 1.5 * D1  # np.arctan(dd2)/np.pi+0.5 + 1.5*D1
        triptau = 1 #np.exp(ttriptau)
        tripfr = 0 #np.arctan(ttripfr) / np.pi + 0.5
        T1 = self.T1
        T2 = self.T2
        Y = np.arctan(y) / np.pi + 0.5

        # self.iterpara.append((N,D1,D2,tripfr,triptau,Y))
        self.N.append(N)
        self.D1.append(D1)
        #self.D2.append(D2)
        self.tripfr.append(tripfr)
        self.triptau.append(triptau)
        self.Y.append(Y)

        #print(N, D1, w, tripfr, triptau, Y)  # (1-Y) -> (Y) + võtsin ära d2 printist
        g = 1 / N * ((1) * np.exp(-(T1) / (w ** 2 + 4 * D1 * np.abs(T2))) * (
                    1 + (4 * D1 * (np.abs(T2))) / w ** 2) ** (-1) * (
                                 1 + (4 * D1 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2)*(1 + tripfr * np.exp(-T2 / triptau) / (1 - tripfr)))#+
                     #(Y) * np.exp(-(T1) / (w ** 2 + 4 * D2 * np.abs(T2))) * (1 + (4 * D2 * (np.abs(T2))) / w ** 2) ** (
                     #    -1) * (1 + (4 * D2 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2)) * (
                     #   1 + tripfr * np.exp(-T2 / triptau) / (1 - tripfr))
        return g  # np.asarray((n,d1,d2,tripf,tript),dtype = list)

    def target2(self, x, N, dd1, dd2, ttripfr, ttriptau, y): # open w parameter with triplet state - disabled in fit
        # w = 0.35#mikromeeter
        alpha = 4  # ilma ühikuta
        # dx = a1x #mikromeeter
        # dy = a1y #mikromeeter
        # tp = np.float(tl1) #dwell time s
        # tl = np.float(tp1) #line time s
        w = 0.2856 # 0.26
        D1 = np.exp(dd1)  # np.exp(dd1) #np.arctan(dd1)/np.pi +0.5 # np.exp
        D2 = (np.arctan(dd2) / np.pi + 0.5) * 200 + 1.5 * D1  # (np.arctan(dd2)/np.pi + 0.5) + 1.5*D1 #1
        triptau = np.exp(ttriptau)
        tripfr = np.arctan(ttripfr) / np.pi + 0.5
        T1 = self.T1
        T2 = self.T2
        Y = np.arctan(y) / np.pi + 0.5  # 0.1
        #print(N, D1, D2, w, tripfr, triptau, Y)
        g = 1 / N * (
                (1 - Y) * np.exp(-(T1) / (w ** 2 + 4 * D1 * np.abs(T2))) * (1 + (4 * D1 * (np.abs(T2))) / w ** 2) ** (
            -1) * (1 + (4 * D1 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2) +
                (Y) * np.exp(-(T1) / (w ** 2 + 4 * D2 * np.abs(T2))) * (1 + (4 * D2 * (np.abs(T2))) / w ** 2) ** (
                    -1) * (1 + (4 * D2 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2)) * (
                    1 + tripfr * np.exp(-T2 / triptau) / (1 - tripfr))
        self.N.append(N)
        self.D1.append(D1)
        self.D2.append(D2)
        self.tripfr.append(tripfr)
        self.triptau.append(triptau)
        self.Y.append(Y)
        return g

    def target3(self, x, N, dd1, ttripfr, ttriptau, y):
        # w = 0.35#mikromeeter
        alpha = 4  # ilma ühikuta
        # dx = a1x #mikromeeter
        # dy = a1y #mikromeeter
        # tp = np.float(tl1) #dwell time s
        # tl = np.float(tp1) #line time s
        w = 0.2856  # 0.26
        D1 = np.exp(dd1)  # np.exp(dd1)#np.arctan(dd1)/np.pi+0.5
        #D2 = (np.arctan(dd2) / np.pi + 0.5) * 200 + 1.5 * D1  # np.arctan(dd2)/np.pi+0.5 + 1.5*D1
        triptau = np.exp(ttriptau)
        tripfr = np.arctan(ttripfr) / np.pi + 0.5
        T1 = self.T1
        T2 = self.T2
        Y = np.arctan(y) / np.pi + 0.5

        # self.iterpara.append((N,D1,D2,tripfr,triptau,Y))
        self.N.append(N)
        self.D1.append(D1)
        #self.D2.append(D2)
        self.tripfr.append(tripfr)
        self.triptau.append(triptau)
        self.Y.append(Y)

        #print(N, D1, w, tripfr, triptau, Y)  # (1-Y) -> (Y) + võtsin ära d2 printist
        g = 1 / N * ((1) * np.exp(-(T1) / (w ** 2 + 4 * D1 * np.abs(T2))) * (
                    1 + (4 * D1 * (np.abs(T2))) / w ** 2) ** (-1) * (
                                 1 + (4 * D1 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2)*(1 + tripfr * np.exp(-T2 / triptau) / (1 - tripfr)))#+
                     #(Y) * np.exp(-(T1) / (w ** 2 + 4 * D2 * np.abs(T2))) * (1 + (4 * D2 * (np.abs(T2))) / w ** 2) ** (
                     #    -1) * (1 + (4 * D2 * (np.abs(T2))) / (alpha ** 2 * w ** 2)) ** (-1 / 2)) * (
                     #   1 + tripfr * np.exp(-T2 / triptau) / (1 - tripfr))
        return g  # np.asarray((n,d1,d2,tripf,tript),dtype = list)

def fit(corr, T2, T1, t):  # concatenate, T2cat, T1cat, corcat
    tgt = Task(T1, T2)  # N, dd1, dd2, ww, ttripfr, ttriptau,y

    print(T1.shape)
    print(corr.shape)
    #N, dd1, dd2, ttripfr, ttriptau, y
    #popt = curve_fit(tgt.target1, T1, corr, [1, 1, -1], factor=0.01, maxfev=180000)
    #popt2 = curve_fit(tgt.target2, T1, corr, [1.6096, 1, -3.0130, -0.0087, -13.815, 1], factor=0.01,
    #                  maxfev=180000)

    if t==1:
        popt3 = curve_fit(tgt.target1, T1, corr, [1.6096, 1, -0.0087, -13.815, 1], factor=0.001, maxfev=180000)
        c, dz = popt3
        v = tgt.target1(T1, *c) - corr
        res1, res2, res3 = np.array_split(v, 3)

        N = tgt.N
        D1 = tgt.D1
        tripfr = tgt.tripfr
        triptau = tgt.triptau
        Y = tgt.Y

        result = dict(N = tgt.N[-1], D1 = tgt.D1[-1])

        plot4 = difusionplot1(a1sum, corr1, c, x, tgt1, 1, 'w. chosen w', N, D1, tripfr, triptau, Y, NI, res1, 0)

        plot5 = difusionplot1(a2sum, corr2, c, x, tgt2, 1, 'w. chosen w', N, D1, tripfr, triptau, Y, NI, res2, 0)
        # plot51 = difusionplot1(a2sum,a2,c2,x2,tgt2,1,'w.o. open w and solo calc')

        plot6 = difusionplot1(a3sum, corr3, c, x, tgt3, 1, 'w. chosen w', N, D1, tripfr, triptau, Y, NI, res3, 1)
    if t ==2:
        popt2 = curve_fit(tgt.target2, T1, corr, [1.6096, 1, -3.0130, -0.0087, -13.815, 1], factor=0.01,
                                            maxfev=180000)
        c, cz = popt2
        # c, dz = popt3
        v1 = tgt.target2(T1,*c) - corr
        res4, res5, res6 = np.array_split(v1,3)

        N = tgt.N
        D1 = tgt.D1
        D2 = tgt.D2
        tripfr = tgt.tripfr
        triptau = tgt.triptau
        Y = tgt.Y

        result = dict(N=tgt.N[-1], D1=tgt.D1[-1], D2=tgt.D2[-1],
                      TripletFraction=tgt.tripfr[-1],
                      TripletTau=tgt.triptau[-1],
                      D2_Fraction=tgt.Y[-1], N_Fraction = variable)

        plot4 = difusionplot2(a1sum, corr1, c, x, tgt1, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI, res4, 0)

        plot5 = difusionplot2(a2sum, corr2, c, x, tgt2, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI, res5, 0)

        plot6 = difusionplot2(a3sum, corr3, c, x, tgt3, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI, res6, 1)

        D1 = [D1,D2]
        
    if t == 3:
        popt3 = curve_fit(tgt.target3, T1, corr, [1.6096, 3, -0.0087, -13.815, 1], factor=0.001, maxfev=180000)
        c, dz = popt3
        v = tgt.target3(T1, *c) - corr
        res1, res2, res3 = np.array_split(v, 3)

        N = tgt.N
        D1 = tgt.D1
        tripfr = tgt.tripfr
        triptau = tgt.triptau
        Y = tgt.Y

        result = dict(N = tgt.N[-1], D1 = tgt.D1[-1],
                      TripletFraction=tgt.tripfr[-1], TripletTau=tgt.triptau[-1], N_Fraction = variable)

        plot4 = difusionplot1(a1sum, corr1, c, x, tgt1, 1, 'w. chosen w', N, D1, tripfr, triptau, Y, NI, res1, 0)

        plot5 = difusionplot1(a2sum, corr2, c, x, tgt2, 1, 'w. chosen w', N, D1, tripfr, triptau, Y, NI, res2, 0)
        # plot51 = difusionplot1(a2sum,a2,c2,x2,tgt2,1,'w.o. open w and solo calc')

        plot6 = difusionplot1(a3sum, corr3, c, x, tgt3, 1, 'w. chosen w', N, D1, tripfr, triptau, Y, NI, res3, 1)


    return result, plot4, plot5, plot6#, basedata



def difusionplot1(data,corr, b, x1, tgt, N, text, Ni, D1, tripfr, triptau, Y, NI,residual, u):

    plot, (ax1, ax2, ax3, ax4) = plt.subplots(4, squeeze='False', gridspec_kw={'height_ratios': [3, 1, 3, 3]})
    ax1.set_title(filename[-9:-3] + ' ' + text + ' '+str((data.shape[0],data.shape[1])))
    xsum = N + len(x1)
    fit = np.flip(np.array_split(np.flip(tgt.target3(x1, *b)),NI))
    xi = range(NN)
    ax1.plot(xi[1:],np.flip(fit[0]) ,label = 'Fit 4')
    for i in range(1,NI):
        ax1.plot(xi,np.flip(fit[i]), color = 'tab:blue')

    # x1 = np.arange(0,NN)
    #ax1.plot(x1, tgt.target1(x1, *a), label="Fit")
    #ax1.plot(x1[:(NI-1)*NN-1], tgt.target3(x1, *b)[:(NI-1)*NN-1], label=("Fit 4"))#, tau =" + str(round(np.exp(b[4]), 8)) + " tripfr=" + str(
        #round((np.arctan(b[3]) / np.pi + 0.5), 4))))
    corre = np.flip(np.array_split(np.flip(corr),NI))

    for i in range(NI):
        if i == 0:
            ax1.plot(xi[1:], np.flip(corre[0]),
                    label=((data.shape[0], data.shape[1]), "y=" + str(i)))
        else:
            ax1.plot(xi, np.flip(corre[i]),
                    label=((data.shape[0], data.shape[1]), "y="+str(i)))

    ax1.set(xscale = 'log', xlabel='Spatial lag (pixels)', ylabel=r"G($\xi$,y)")
    ax1.legend(loc='upper right', fontsize='small')

    for i in range(NI):
        if i ==0:
            print(i)
            ax2.plot(x1[:NN - 1],
                     residual[1:NN])

        elif i != NI-1:
            ax2.plot(x1[i*NN:(i+1) * NN - 1],
                     residual[i*NN + 1:(i+1) * NN])
            print(i)
        if i == NI-1:
            if u == 1:
                print(i)
                ax2.plot(x1[i*NN:(i+1) * NN - 1],
                        residual[i*NN:(i+1) * NN])
            else:
                print(i)
                ax2.plot(x1[i * NN:(i + 1) * NN - 1],
                         residual[i * NN:(i + 1) * NN])

    ax2.plot(x1, np.zeros(len(x1)))
    ax2.set(xscale='log')

    ax4.plot(Ni, label='N')
    ax3.plot(D1, label='D1')
    #ax3.plot(D2, label='D2')
    ax4.plot(tripfr, label='tripfr')
    ax4.plot(triptau, label='triptau')
    ax4.plot(Y, label='Y')
    ax3.legend(loc='upper right', fontsize='small')
    ax4.legend(loc='upper right', fontsize='small')
    plt.subplots_adjust(left=0.1,
                        bottom=0.1,
                        right=0.9,
                        top=0.9,
                        wspace=0.4,
                        hspace=0.4)
    return plot

def difusionplot2(data,corr, b, x1, tgt, N, text, Ni, D1,D2, tripfr, triptau, Y, NI,residual, u):
    print(D2)
    plot, (ax1, ax2, ax3, ax4) = plt.subplots(4, squeeze='False', gridspec_kw={'height_ratios': [3, 1, 3, 3]})
    ax1.set_title(filename[-9:-3] + ' ' + text + ' '+str((data.shape[0],data.shape[1])))
    xsum = N + len(x1)
    fit = np.flip(np.array_split(np.flip(tgt.target2(x1, *b)),NI))
    xi = range(NN)
    ax1.plot(xi[1:],np.flip(fit[0]) ,label = 'Fit 4')
    for i in range(1,NI):
        ax1.plot(xi,np.flip(fit[i]), color = 'tab:blue')

    # x1 = np.arange(0,NN)
    #ax1.plot(x1, tgt.target1(x1, *a), label="Fit")
    #ax1.plot(x1[:(NI-1)*NN-1], tgt.target3(x1, *b)[:(NI-1)*NN-1], label=("Fit 4"))#, tau =" + str(round(np.exp(b[4]), 8)) + " tripfr=" + str(
        #round((np.arctan(b[3]) / np.pi + 0.5), 4))))
    corre = np.flip(np.array_split(np.flip(corr),NI))

    for i in range(NI):
        if i == 0:
            ax1.plot(xi[1:], np.flip(corre[0]),
                    label=((data.shape[0], data.shape[1]), "y=" + str(i)))
        else:
            ax1.plot(xi, np.flip(corre[i]),
                    label=((data.shape[0], data.shape[1]), "y="+str(i)))

    ax1.set(xscale = 'log', xlabel='Spatial lag (pixels)', ylabel=r"G($\xi$,y)")
    ax1.legend(loc='upper right', fontsize='small')

    for i in range(NI):
        if i ==0:
            print(i)
            ax2.plot(x1[:NN - 1],
                     residual[1:NN])

        elif i != NI-1:
            ax2.plot(x1[i*NN:(i+1) * NN - 1],
                     residual[i*NN + 1:(i+1) * NN])
            print(i)
        if i == NI-1:
            if u == 1:
                print(i)
                ax2.plot(x1[i*NN:(i+1) * NN - 1],
                        residual[i*NN:(i+1) * NN])
            else:
                print(i)
                ax2.plot(x1[i * NN:(i + 1) * NN - 1],
                         residual[i * NN:(i + 1) * NN])

    ax2.plot(x1, np.zeros(len(x1)))
    ax2.set(xscale='log')

    ax4.plot(Ni, label='N')
    ax3.plot(D1, label='D1')
    ax3.plot(D2, label='D2')
    ax4.plot(tripfr, label='tripfr')
    ax4.plot(triptau, label='triptau')
    ax4.plot(Y, label='Y')
    ax3.legend(loc='upper right', fontsize='small')
    ax4.legend(loc='upper right', fontsize='small')
    plt.subplots_adjust(left=0.1,
                        bottom=0.1,
                        right=0.9,
                        top=0.9,
                        wspace=0.4,
                        hspace=0.4)
    return plot

size = []
for i in data:
    dataimage = i.corre
    mean = i.mean
    dataimage = dataimage/mean**2
    linetime = i.linet
    pixeltime = i.pixelt
    pixelsizex = i.pixelsizex
    pixelsizey = i.pixelsizey
    corr, T2, T1, x = diffusiondata(dataimage, linetime, pixeltime, pixelsizex, pixelsizey, NI)
    tgt = Task(T1, T2)
    size.append(2*dataimage.shape)
    popt3 = curve_fit(tgt.target1, T1, corr, [1.6096, 1, -0.0087, -13.815, 1], factor=0.001, maxfev=180000)
    c, dz = popt3

    #v1 = tgt.target3(T1, *c) - corr


    #N = tgt.N
    #D1 = tgt.D1
    #D2 = tgt.D2
    #tripfr = tgt.tripfr
    #triptau = tgt.triptau
    #Y = tgt.Y


    #plot4 = difusionplot1(data, corr, c, x, tgt, 1, 'w. chosen w', N, D1,tripfr, triptau, Y, NI, v1, 0)


    #plt.show()
    #plot5 = difusionplot2(a2sum, corr2, c, x, tgt2, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI, res5, 0)

    #plot6 = difusionplot2(a3sum, corr3, b, x, tgt3, 1, 'w. chosen w', N, D1, D2, tripfr, triptau, Y, NI, res6, 1)

    #print(c)
    dif.append(np.exp(c[1]))
    filterN.append(c[0])


meandif = np.mean(dif)

#print(len(dif))
#print(len(size))

def difseparate(dif, meandif):
    difsep = []
    regdif = []
    ident = []
    a = 0
    for i in dif:
        #if np.absolute(i-meandif) <= 2.5:
        #if i-meandif <= -5:
        if i > variable:
            regdif.append(i)
            difsep.append(meandif)
            ident.append(a)
        else:
            regdif.append(meandif)
            difsep.append(i)
        a += 1
    return difsep, regdif, ident

def nseparate(dif, meandif, filterN):
    difsep = []
    regdif = []
    ident = []
    a = 0
    u = 0
    for k in range(len(filterN)):
        i = dif[k]
        n = filterN[k]
        if n > variable:
            regdif.append(i)
            difsep.append(meandif)
            ident.append(a)
        else:
            regdif.append(meandif)
            difsep.append(i)
            u +=1
        a += 1
    return difsep, regdif, ident, u/a

#difsep, regdif, index = difseparate(dif,meandif)
difsep, regdif, index , imagesremoved = nseparate(dif,meandif,filterN)

def difplot(dif1,dif2,filterN,size, dif):

    aug, ((ax1,ax2,ax3),(ax4,ax5,ax6)) = plt.subplots(2,3) # squeeze='False', gridspec_kw={'height_ratios': [2, 2, 2,2,2,2]})
    ax1.plot(dif1)
    ax1.plot(dif2)
    ax2.plot(filterN)
    ax3.plot(size)
    ax4.hist(filterN)
    ax5.hist(dif)
    ax6.plot(filterN,dif,'.')
    aug.suptitle("Plots Dif vs difsep, N, size, Nhist, difhist, N(dif), N = " + str(variable))
    #plt.show()

    return aug

differplot = difplot(difsep,regdif,filterN,size,dif)


def shape(data , index):
    grouped_data = collections.defaultdict(list)
    for i in range(len(data)):
        if i in index:
            grouped_data[str(data[i].image.shape)].append(data[i])

    return grouped_data

group_data = shape(data,index)

keys = group_data.keys()

print('Groups', keys)

a = 0
for i in keys:
    aver = 0
    for u in range(len(group_data[i])):
        aver += group_data[i][u].corre / (group_data[i][u].mean**2)
    aver /= len(group_data[i])
    if a == 0:
        a1sum = aver
    if a == 1:
        a2sum = aver
    if a == 2:
        a3sum = aver
    a+=1

a= 0
for i in keys:
    if a== 0:
        corr1, t21, t11, x1 = diffusiondata(a1sum, group_data[i][0].linet, group_data[i][0].pixelt, group_data[i][0].pixelsizex, group_data[i][0].pixelsizey, NI)
    if a == 1:
        corr2, t22, t12, x2 = diffusiondata(a2sum, group_data[i][0].linet, group_data[i][0].pixelt, group_data[i][0].pixelsizex, group_data[i][0].pixelsizey, NI)
    if a == 2:
        corr3, t23, t13, x3 = diffusiondata(a3sum, group_data[i][0].linet, group_data[i][0].pixelt, group_data[i][0].pixelsizex, group_data[i][0].pixelsizey, NI)
    a+=1


corr = np.concatenate([corr1, corr2, corr3])

Te2 = np.concatenate([t21, t22, t23])

Te1 = np.concatenate([t11, t12, t13])

x = np.concatenate([x1, x2, x3])

tgt = Task(Te2, Te1)
tgt1 = Task(t11, t21)
tgt2 = Task(t12, t22)
tgt3 = Task(t13, t23)

rtgt3, plot1, plot2, plot3= fit(corr, Te2, Te1, 3)

rtgt2, plot4, plot5, plot6= fit(corr, Te2, Te1, 2)

#plot8 = difplot1(corr1,'Size1',t11,t21)

#plot9 = difplot1(corr2,'Size2',t12,t22)

#plot10 = difplot1(corr3,'Size3',t13,t23)

#plt.show()

def datafile(data, csvname, imagesremoved):
    fname = os.path.basename(filename)[:-3]
    dname = os.path.dirname(filename)
    data['Images Removed'] = imagesremoved
    keys = list(data.keys())
    data['Proov'] = fname
    keys.insert(0, 'Proov')
    if fname == "Proov1":
        with open(os.path.join(dname, csvname), mode='w') as data_file:
            data_writer = csv.writer(data_file, delimiter=',')
            data_writer.writerow(keys)
            data_file.close()
    with open(os.path.join(dname, csvname), mode='a') as data_file:
        data_writer = csv.writer(data_file, delimiter=',')
        data_writer.writerow([data[k] for k in keys])

datafile(rtgt3, 'Proovidata4single.csv', imagesremoved)
datafile(rtgt2, 'Proovidata4dual.csv', imagesremoved)



#self.N.append(N)
#self.D1.append(D1)
# self.D2.append(D2)
#self.tripfr.append(tripfr)
#self.triptau.append(triptau)
#self.Y.append(Y)


pp = PdfPages(str(filename[:-3])+'_analyzeN.pdf')
pp.savefig(differplot)
pp.savefig(plot1)
pp.savefig(plot2)
pp.savefig(plot3)
pp.savefig(plot4)
pp.savefig(plot5)
pp.savefig(plot6)
pp.close()



#for i in range(len(dif)):
#    if dif[i]-meandif >5 or
#plt.figure()
#plt.plot(dif)

#plt.show()

#tgt = Task(T1, T2)  # N, dd1, dd2, ww, ttripfr, ttriptau,y

#print(T1.shape)
#print(corr.shape)
#N, dd1, dd2, ttripfr, ttriptau, y
    #popt = curve_fit(tgt.target1, T1, corr, [1, 1, -1], factor=0.01, maxfev=180000)
#popt2 = curve_fit(tgt.target2, T1, corr, [1.6096, 1, -3.0130, -0.0087, -13.815, 1], factor=0.01,
#                  maxfev=180000)
    #popt3 = curve_fit(tgt.target3, T1, corr, [1.6096, 1, -0.0087, -13.815, 1], factor=0.001, maxfev=180000)
    #a, bz = popt
#b, cz = popt2
    #c, dz = popt3
#    v1 = tgt.target2(T1,*b) - corr
#    res4, res5, res6 = np.array_split(v1,3)
    #v = tgt.target3(T1, *c) - corr
    #res1, res2, res3 = np.array_split(v,3)


    # N,D1,D2,tripfr,triptau,Y
 #   N = tgt.N
  #  D1 = tgt.D1
   # D2 = tgt.D2
   # tripfr = tgt.tripfr
   # triptau = tgt.triptau
   # Y = tgt.Y

   # print("#F")
   # print(D1)
   # print(D2)
