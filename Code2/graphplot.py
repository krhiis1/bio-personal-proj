import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv
import argparse
from dataseparate import dataseparater
import pickle
import collections
from matplotlib.backends.backend_pdf import PdfPages


parser = argparse.ArgumentParser(description='Read in the filename')
parser.add_argument('filepath', type= str)
parser.add_argument('filepath2', type= str)
parser.add_argument('datavar', metavar = 'N.N', type= float, nargs = '?', const = -1, default =2)
args = parser.parse_args()

filename = args.filepath
filename2 = args.filepath2
variable = args.datavar

data = pickle.load(open(filename[:-3]+"_calcdata", "rb"))

data1 = pickle.load(open(filename[:-3]+"_singleplot", "rb"))
data2 = pickle.load(open(filename[:-3]+"_dualplot", "rb"))
keys1 = list(data1.keys())
keys2 = list(data2.keys())

data0 = pickle.load(open(filename2[:-3]+"_calcdata", "rb"))

data3 = pickle.load(open(filename2[:-3]+"_singleplot", "rb"))
data4 = pickle.load(open(filename2[:-3]+"_dualplot", "rb"))
keys3 = list(data3.keys())
keys4 = list(data4.keys())

print(len(data))

image = []
for i in data:
    image.append(i.image)

image0 = []
for i in data0:
    image0.append(i.image)

index = data1["index"][0][0]
index0 = data3["index"][0][0]

dataimage = []
for i in data:
    dataimage.append(i.corre)

dataimage2 = []
for j in data0:
    dataimage2.append(j.corre)

image2 = [] #regular image
image22 = [] #Correlation
for i in range(len(image)):
    if i in index:
        continue
    else:
        image2.append(image[i])
        image22.append(dataimage[i])

image20 = []
image220 = []
for i in range(len(image0)):
    if i in index0:
        continue
    else:
        image20.append(image[i])
        image220.append(dataimage2[i])



print("tühi")
print(len(image2))
print(len(image22))
print(len(image220))
print(image2[0].shape)
print(len(image))
print("kaks")
#print(data1[keys1[0]][0])

def imageplot(image, image2,file):
    pp = PdfPages(str(file[:-3])+'images.pdf')
    for i in range(len(image)):
        plot, (ax1, ax2) = plt.subplots(2)
        ax1.imshow(image[i].T)
        ax2.imshow(image2[i].T, vmin = 0, vmax = 0.1)
        pp.savefig(plot)

    pp.close()
#print(keys1)
#print(keys2)

imageplot(image2, image22,filename)
imageplot(image20, image220, filename2)

def dataplot(data,data1 , keys, keys1, images, images1, text):
    plot1data = [data[keys[0]][0], data[keys[0]][1], data[keys[0]][2]]
    plot2data = [data[keys[1]][0], data[keys[1]][1], data[keys[1]][2]]
    plot3data = [data[keys[2]][0], data[keys[2]][1], data[keys[2]][2]]
    plot4data = [data1[keys1[0]][0], data1[keys1[0]][1], data[keys1[0]][2]]
    plot5data = [data1[keys1[1]][0], data1[keys1[1]][1], data[keys1[1]][2]]
    plot6data = [data1[keys1[2]][0], data1[keys1[2]][1], data[keys1[2]][2]]

    filtern1 = [data["filter"][0][0]]
    filtern2 = [data1["filter"][0][0]]
    dif1 = [data["filter"][1][0]]
    dif2 = [data1["filter"][1][0]]

    sizes1 = sorted([plot1data[2][0][0],plot2data[2][0][0],plot3data[2][0][0]])
    sizes2 = sorted([plot4data[2][0][0],plot5data[2][0][0],plot6data[2][0][0]])

    a = 0
    b = 0
    c = 0

    print(plot1data[2][0])
    print(sizes1)
    print(sizes2)
    print("igj")

    for i in range(len(sizes1)):
        if sizes1[i] == plot1data[2][0][0]:
            print("1")
            if a==0:
                a = plot1data
            elif b==0:
                b = plot1data
            elif c==0:
                c= plot1data
        elif sizes1[i] == plot2data[2][0][0]:
            print("2")
            if a==0:
                a = plot2data
            elif b==0:
                b = plot2data
            elif c==0:
                c= plot2data
        elif sizes1[i] == plot3data[2][0][0]:
            print("3")
            if a==0:
                a = plot3data
            elif b==0:
                b = plot3data
            elif c==0:
                c= plot3data

    d = 0
    e = 0
    f = 0

    for i in range(len(sizes2)):
        if sizes2[i] == plot4data[2][0][0]:
            print("4")
            if d==0:
                d = plot4data
            elif e==0:
                e = plot4data
            elif f==0:
                f = plot4data
        elif sizes2[i] == plot5data[2][0][0]:
            print("5")
            if d==0:
                d = plot5data
            elif e==0:
                e = plot5data
            elif f==0:
                f = plot5data
        elif sizes2[i] == plot6data[2][0][0]:
            print("6")
            if d==0:
                d = plot6data
            elif e==0:
                e = plot6data
            elif f==0:
                f = plot6data

    #print(plot1data[0])
    #print("b")
    #print(plot1data[1])
    #print("a")
    #print(len(plot1data[1]))

#plot1
    x = range(len(plot1data[0][0]))
    plot1, ax1 = plt.subplots()
    ax1.plot(x, a[0][0], 'b')#plot1data[0][0], 'b')
    ax1.plot(x, a[1][0], 'bo', markersize = 3)#plot1data[1][0], 'bo')
    ax1.plot(x, b[0][0], 'g')#plot2data[0][0], 'g')
    ax1.plot(x, b[1][0], 'go', markersize = 3)#plot2data[1][0], 'go')
    ax1.plot(x, c[0][0], 'r')#plot3data[0][0], 'r')
    ax1.plot(x, c[1][0], 'ro', markersize = 3)#plot3data[1][0], 'ro')
    ax1.set(xscale = "log")
    ax1.set(xlabel = r"$\Delta t , \mu s$", ylabel = "Correlation, relative")

    print("kif")
    print(a[0][0][0])
    print(b[0][0][0])
    print(c[0][0][0])
    print(d[0][0][0])
    print(e[0][0][0])
    print(f[0][0][0])
    print("kauf")

#plot2
    plot2, ax2= plt.subplots()
    ax2.plot(x, d[0][0], 'b')
    ax2.plot(x, d[1][0], 'bo', markersize = 3)
    ax2.plot(x, e[0][0], 'g')
    ax2.plot(x, e[1][0], 'go', markersize = 3)
    ax2.plot(x, f[0][0], 'r')
    ax2.plot(x, f[1][0], 'ro', markersize = 3)
    ax2.set(xscale="log")
    ax2.set(xlabel= r"$\Delta t , \mu s$", ylabel="Correlation, relative")

#plot3
    plot3, ax3 = plt.subplots()
    ax3.plot(filtern1,dif1,'b.',markersize = 1, alpha = 0.2)
    ax3.set(xlabel = "N , Count", ylabel = r"Diffusion Coefficient, $m/s^2$")
    ax3.set_xlim(left = 0, right = 2.5)
    ax3.set_ylim(bottom = 0, top = 200)

#plot4
    plot4, ax4 = plt.subplots()
    ax4.plot(filtern2,dif2,'b.',markersize = 1, alpha = 0.2)
    ax4.set(xlabel = "N , Count", ylabel = r"Diffusion Coefficient, $m/s^2$")
    ax4.set_xlim(left=0, right=2.5)
    ax4.set_ylim(bottom=0, top=100)

    print("kui")
    #print(type(images[33]))
    print("tore")
#plot5
    plot5, ax5 = plt.subplots()
    ax5.imshow(images[10].transpose(1,0))
    ax5.set(xlabel = "x, pixels", ylabel = "y, pixels")
#plot6
    #plot6, ax6 = plt.subplots()
    #ax6.imshow(images1[60])

    fu = open(filename[:-9]+text+"graphdata.csv", 'w', newline ="")

    writer = csv.writer(fu)
    writer.writerow([filename[-9:-3],[" "], [" "], [" "], [" "], [" "],filename2[-9:-3]])
    writer.writerow(["paaris kiiruste kombinatsioonis, 1 veerg correlation, 2 veerg fit"])
    row = [sizes1[0],sizes1[0],sizes1[1],sizes1[1],sizes1[2],sizes1[2],sizes2[0],sizes2[0],sizes2[1],sizes2[1],sizes2[2],sizes2[2]]
    writer.writerow(row)
    for i in range(len(a[0][0])):
        row1 = [a[0][0][i],a[1][0][i],b[0][0][i],b[1][0][i],c[0][0][i],c[1][0][i],d[0][0][i],d[1][0][i],e[0][0][i],e[1][0][i],f[0][0][i],f[1][0][i]]
        writer.writerow(row1)


    fu.close()

    return plot1, plot2, plot3, plot4, plot5


plot1, plot2, plot3, plot4, plot5= dataplot(data1,data3, keys1, keys3, image2, image20, "single")
plot6, plot7, plot8, plot9, plot10 = dataplot(data2,data4, keys2, keys4, image2, image20, "dual")

#plt.show()

pp1 = PdfPages(str(filename[:-3])+'_singlegraphs.pdf')
pp1.savefig(plot1)
pp1.savefig(plot2)
pp1.savefig(plot3)
pp1.savefig(plot4)
pp1.savefig(plot5)
pp1.close()

print("fik")

pp2 = PdfPages(str(filename[:-3])+'_dualgraphs.pdf')
pp2.savefig(plot6)
pp2.savefig(plot7)
pp2.savefig(plot8)
pp2.savefig(plot9)
pp2.savefig(plot10)
pp2.close()
