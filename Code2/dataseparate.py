
import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import csv
import argparse





class dataseparater:
    def __init__(self, data, pixelsizex, pixelsizey, linet, pixelt, shape):
        self.image = data
        self.pixelsizex = pixelsizex
        self.pixelsizey = pixelsizey
        self.linet = linet
        self.pixelt = pixelt
        self.shape = shape
        self.mean = None
        self.corre = None

    def padding(self, mean):
        d = (self.image-mean)
        self.padded = np.pad(d, (
                    (d.shape[0] // 2, d.shape[0] // 2),
                    (d.shape[1] // 2, d.shape[1] // 2)),
                    constant_values=0.0)

    def calcsum(self):
        return np.sum(self.image), self.image.shape[0]*self.image.shape[1]

    def factorcalc(self):
        yhed = np.ones((self.image.shape[0], self.image.shape[1]))
        yhed = np.pad(yhed, ((yhed.shape[0] // 2, yhed.shape[0] // 2), (yhed.shape[1] // 2, yhed.shape[1] // 2)),
                      constant_values=0.0)
        return signal.correlate(yhed, yhed, mode="same")[int(yhed.shape[0])//4:int(yhed.shape[0])*3//4,
                int(yhed.shape[1])//4:int(yhed.shape[1])*3//4]


    def correlationcalc(self):
        factor = self.factorcalc()
        corre = signal.correlate(self.padded, self.padded, mode="same")[int(self.padded.shape[0])//4:int(self.padded.shape[0])*3//4,
                int(self.padded.shape[1])//4:int(self.padded.shape[1])*3//4]
        #print(self.image.shape, self.shape, corre.shape, factor.shape)
        self.corre = corre / factor
        return None
