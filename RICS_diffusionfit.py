import csv 
import h5py
import inspect
import os
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.ndimage import uniform_filter1d
import collections
import sys

maxInt = sys.maxsize

while True:
    # decrease the maxInt value by factor 10 
    # as long as the OverflowError occurs.

    try:
        csv.field_size_limit(maxInt)
        break
    except OverflowError:
        maxInt = int(maxInt/10)

os.chdir("/Users/hiis-/Desktop/HDF5/Bio")

row1 = []
row2 = []
row3 = []

filename = 'proov1_parameters.csv'

with open(filename,newline = "") as f:
    reader = csv.reader(f)     

    for i, row in enumerate(reader):
        if i == 0: 
            data = [[] for _ in range(len(row))]

        for j, col in enumerate(row):
            data[j].append(col) 

with open('proov1_data.csv',newline = "") as f:
    reader = csv.reader(f)     

    for i, row in enumerate(reader):
        if i == 0: 
            data1 = [[] for _ in range(len(row))]

        for j, col in enumerate(row):
            data1[j].append(col) 

a1sumin = data1[0][1].split(", ")

a2sumin = data1[1][1].split(", ")

a3sumin = data1[2][1].split(", ")

a1sum = []
a2sum = []
a3sum = []

a1sumin[0] = a1sumin[0][1:]
a1sumin[-1] = a1sumin[-1][:-2]

a2sumin[0] = a1sumin[0][1:]
a2sumin[-1] = a1sumin[-1][:-2]

a3sumin[0] = a1sumin[0][1:]
a3sumin[-1] = a1sumin[-1][:-2]

a1sum = np.asarray(a1sumin).astype(np.float)

a2sum = np.asarray(a2sumin).astype(np.float)

a3sum = np.asarray(a3sumin).astype(np.float)

size1x = int(data1[3][1])
size1y = int(data1[4][1])
size2x = int(data1[5][1])
size2y = int(data1[6][1])
size3x = int(data1[7][1])
size3y = int(data1[8][1])

a1sum = a1sum.reshape((size1x,size1y))
a2sum = a2sum.reshape((size2x,size2y))
a3sum = a3sum.reshape((size3x,size3y))

linetime = data[0]
pixeltime = data[1]
pixelsizex = data[2]
pixelsizey = data[3]

tl1 = float(linetime[1])
tp1 = float(pixeltime[1])
a1x = float(pixelsizex[1])
a1y = float(pixelsizey[1])

tl2 = float(linetime[2])
tp2 = float(pixeltime[2])
a2x = float(pixelsizex[2])
a2y = float(pixelsizey[2])

tl3 = float(linetime[3])
tp3 = float(pixeltime[3])
a3x = float(pixelsizex[3])
a3y = float(pixelsizey[3])

#%% Diffusion
import PIL

NN = 200

def diffusiondata(data, linetime, pixeltime,pixelsizex, pixelsizey):
    
    

    x = np.arange(0,NN)
    x = np.concatenate((x,x,x,x,x))
    x = x.astype(np.float)
    
    y = np.concatenate((0*np.ones(NN),1*np.ones(NN),2*np.ones(NN),3*np.ones(NN),4*np.ones(NN)))
    y = y.astype(np.float)
    
    
    corr = data[data.shape[0]//2:data.shape[0]//2+NN,data.shape[1]//2:data.shape[1]//2+5].ravel(order ="F")

    corr = corr[1:]
    x = x[1:]
    y = y[1:]
    
    T2 = pixeltime*x + linetime*y
    
    tx = pixelsizex*x
    ty = pixelsizey*y
    
    T1 = tx**2 + ty**2
    
    return corr, T2, T1, x


class Task:
    def __init__(self,T1,T2):        
        self.T1 = T1        
        self.T2 = T2
        
    def target1(self, x, N, dd, ww):
        #w = 0.35#mikromeeter
        alpha = 4 #ilma ühikuta
        #dx = a1x #mikromeeter 
        #dy = a1y #mikromeeter
        #tp = np.float(tl1) #dwell time s
        #tl = np.float(tp1) #line time s
        w = np.exp(ww) #0.26
        D = np.exp(dd)
        T1 = self.T1
        T2 = self.T2
        print(N,D,w)
        g = 1/N*np.exp(-(T1)/(w**2+4*D*np.abs(T2)))*(1+(4*D*(np.abs(T2)))/w**2)**(-1)*(1+(4*D*(np.abs(T2)))/(alpha**2*w**2))**(-1/2)
        return g
    
    def target2(self, x, N, dd, ww, ttripfr, ttriptau):
        #w = 0.35#mikromeeter
        alpha = 4 #ilma ühikuta
        #dx = a1x #mikromeeter 
        #dy = a1y #mikromeeter
        #tp = np.float(tl1) #dwell time s
        #tl = np.float(tp1) #line time s
        w = np.exp(ww) #0.26
        D = np.exp(dd)
        triptau = np.exp(ttriptau)
        tripfr = np.arctan(ttripfr)/np.pi+0.5
        T1 = self.T1
        T2 = self.T2
        print(N,D,w,tripfr, triptau)
        g = 1/N*np.exp(-(T1)/(w**2+4*D*np.abs(T2)))*(1+(4*D*(np.abs(T2)))/w**2)**(-1)*(1+(4*D*(np.abs(T2)))/(alpha**2*w**2))**(-1/2)*(1 + tripfr*np.exp(-T2/triptau)/(1-tripfr))
        return g
    
    def target3(self, x, N, dd, ww, ttripfr, ttriptau):
        #w = 0.35#mikromeeter
        alpha = 4 #ilma ühikuta
        #dx = a1x #mikromeeter 
        #dy = a1y #mikromeeter
        #tp = np.float(tl1) #dwell time s
        #tl = np.float(tp1) #line time s
        w = 0.26
        D = np.exp(dd)
        triptau = np.exp(ttriptau)
        tripfr = np.arctan(ttripfr)/np.pi+0.5
        T1 = self.T1
        T2 = self.T2
        print(N,D,w,tripfr, triptau)
        g = 1/N*np.exp(-(T1)/(w**2+4*D*np.abs(T2)))*(1+(4*D*(np.abs(T2)))/w**2)**(-1)*(1+(4*D*(np.abs(T2)))/(alpha**2*w**2))**(-1/2)*(1 + tripfr*np.exp(-T2/triptau)/(1-tripfr))
        return g
    
def fit(corr, T2, T1): #concatenate, T2cat, T1cat, corcat    
    tgt = Task(T1,T2)
        
    popt = curve_fit(tgt.target1,T1,corr,[1,1,-1], factor=0.01, maxfev = 180000)
    popt2 = curve_fit(tgt.target2,T1,corr,[1,1,-1,0.1,1], factor=0.01, maxfev = 180000)
    popt3 = curve_fit(tgt.target3,T1,corr,[1,1,-1,0.1,1], factor=0.01, maxfev = 180000)
    a,bz = popt
    b,cz = popt2
    c,dz = popt3
    return a,b,c
    #ei tagasta fitti

        

corr1 , t21, t11, x1= diffusiondata(a1sum, tl1, tp1,a1x, a1y)

corr2 , t22, t12, x2 = diffusiondata(a2sum, tl2, tp2,a2x, a2y)

corr3 , t23, t13, x3 = diffusiondata(a3sum, tl3, tp3,a3x, a3y)

corr = np.concatenate((corr1,corr2,corr3))

Te2 = np.concatenate((t21,t22,t23))

Te1 = np.concatenate((t11,t12,t13))

x = np.concatenate((x1,x2,x3))

a, b, c = fit(corr,Te2,Te1)

tgt1 = Task(t11,t21)
tgt2 = Task(t12,t22)
tgt3 = Task(t13,t23)

print(a[0])
print(np.exp(a[1]))
print(np.exp(a[2]))
print("Mg")
print(a)

def difusionplot1(data, a, b,x1,tgt,N):
    plot = plt.figure(str(data)+str(b))
    plt.title((filename[:6],"N = ",round(a[0],4), "D = ", round(np.exp(a[1]),4),"W=",round(np.exp(a[2]),4)))
    #x1 = np.arange(0,NN)
    plt.plot(x1,tgt.target1(x1,*a), label = "Fit")
    plt.plot(x1,tgt.target2(x1,*b), label = ("Fit, tau =" +str(round(np.exp(b[4]),8))+" tripfr="+str(round((np.arctan(b[3])/np.pi+0.5),4))))
    plt.plot(x1,data[data.shape[0]//2+N:data.shape[0]//2+len(x1)+N,data.shape[1]//2], label = ((data.shape[0]//2,data.shape[1]//2), "y=0"))
    plt.plot(x1,data[data.shape[0]//2+N:data.shape[0]//2+len(x1)+N,data.shape[1]//2+1], label = ((data.shape[0]//2,data.shape[1]//2),"y = 1"))
    plt.plot(x1,data[data.shape[0]//2+N:data.shape[0]//2+len(x1)+N,data.shape[1]//2+2], label = ((data.shape[0]//2,data.shape[1]//2), "y = 2"))
    plt.plot(x1,data[data.shape[0]//2+N:data.shape[0]//2+len(x1)+N,data.shape[1]//2+3], label = ((data.shape[0]//2,data.shape[1]//2), "y = 3"))
    plt.xscale("log")
    plt.xlabel("Spatial lag (pixels)")
    plt.ylabel(r"G($\xi$,y)")
    plt.legend()
    plt.ylim(0,1.1)
    return plot

plot1 = difusionplot1(a1sum,a,b,x1,tgt1,1)
plot2 = difusionplot1(a2sum,a,b,x2,tgt2,1)
plot3 = difusionplot1(a3sum,a,b,x3,tgt3,1)

plot4 = difusionplot1(a1sum,a,c,x1,tgt1,1)
plot5 = difusionplot1(a2sum,a,c,x2,tgt2,1)
plot6 = difusionplot1(a3sum,a,c,x3,tgt3,1)
